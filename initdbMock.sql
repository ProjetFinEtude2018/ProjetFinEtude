INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN001', '017', '192.168.1.3', '00:00:00:00:00:01', 'Probleme 1' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN002', '019', '192.168.1.4', '00:00:00:00:00:02', 'Probleme 2' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN003', '019', '192.168.1.5', '00:00:00:00:00:03', 'Probleme 3' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN004', '019', '192.168.1.5', '00:00:00:00:00:04', 'Probleme 4' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN005', '026', '192.168.1.6', '00:00:00:00:00:05', 'Probleme 5' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN006', '026', '192.168.1.7', '00:00:00:00:00:06', 'Probleme 6' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN007', '025', '192.168.1.8', '00:00:00:00:00:07', 'Probleme 7' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN008', '025', '192.168.1.9', '00:00:00:00:00:08', 'Probleme 8' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN009', '025', '192.168.1.10', '00:00:00:00:00:09', 'Probleme 9' , true , '2017-01-01', '2017-01-01');

INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN010', 'B25', '192.168.1.100', '00:00:00:00:00:20', 'Probleme 9' , true , '2017-01-01', '2017-01-01');


INSERT INTO pfe_db.computers(
	name, local, ip, mac_address, comment, active, "createdAt", "updatedAt")
	VALUES ('LEN011', 'B23', '192.168.1.101', '00:00:00:00:00:21', 'Probleme 10' , true , '2017-01-01', '2017-01-01');






/** INSERT BUGS **/

INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud1.stud@student.vinci.be', 'LEN001', 'description Bug 1', 'P');    

    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud1.stud@student.vinci.be', 'LEN010', 'description Bug 2', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud1.stud@student.vinci.be', 'LEN003', 'description Bug 3', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud2.stud@student.vinci.be', 'LEN006', 'description Bug 4', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud3.stud@student.vinci.be', 'LEN004', 'description Bug 5', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud3.stud@student.vinci.be', 'LEN010', 'description Bug 6', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud3.stud@student.vinci.be', 'LEN002', 'description Bug 7', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud4.stud@student.vinci.be', 'LEN001', 'description Bug 8', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud4.stud@student.vinci.be', 'LEN001', 'description Bug 9', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud5.stud@student.vinci.be', 'LEN003', 'description Bug 10', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud6.stud@student.vinci.be', 'LEN004', 'description Bug 11', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud7.stud@student.vinci.be', 'LEN002', 'description Bug 12', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud8.stud@student.vinci.be', 'LEN001', 'description Bug 13', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud9.stud@student.vinci.be', 'LEN010', 'description Bug 14', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud10.stud@student.vinci.be', 'LEN007', 'description Bug 15', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud11.stud@student.vinci.be', 'LEN002', 'description Bug 16', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud12.stud@student.vinci.be', 'LEN001', 'description Bug 17', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud13.stud@student.vinci.be', 'LEN009', 'description Bug 18', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud14.stud@student.vinci.be', 'LEN001', 'description Bug 19', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud15.stud@student.vinci.be', 'LEN006', 'description Bug 20', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud16.stud@student.vinci.be', 'LEN003', 'description Bug 21', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud17.stud@student.vinci.be', 'LEN003', 'description Bug 22', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud18.stud@student.vinci.be', 'LEN002', 'description Bug 23', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud19.stud@student.vinci.be', 'LEN001', 'description Bug 24', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud20.stud@student.vinci.be', 'LEN009', 'description Bug 25', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud21.stud@student.vinci.be', 'LEN010', 'description Bug 26', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud22.stud@student.vinci.be', 'LEN003', 'description Bug 27', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud23.stud@student.vinci.be', 'LEN004', 'description Bug 28', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud24.stud@student.vinci.be', 'LEN005', 'description Bug 29', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud25.stud@student.vinci.be', 'LEN003', 'description Bug 30', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud26.stud@student.vinci.be', 'LEN010', 'description Bug 31', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud27.stud@student.vinci.be', 'LEN008', 'description Bug 32', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud28.stud@student.vinci.be', 'LEN001', 'description Bug 33', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud29.stud@student.vinci.be', 'LEN007', 'description Bug 34', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud30.stud@student.vinci.be', 'LEN001', 'description Bug 35', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud31.stud@student.vinci.be', 'LEN006', 'description Bug 36', 'P');    
    INSERT INTO pfe_db.bugs(
	email_author, name_machine, description, status )
	VALUES ('stud32.stud@student.vinci.be', 'LEN001', 'description Bug 37', 'P');    
