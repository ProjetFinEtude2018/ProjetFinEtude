/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {


  'POST /signup': {
    controller: 'UserController',
    action: 'create'
  },

  'POST /login': {
    controller: 'AuthController',
    action: 'login'
  },

  '/logout': {
    controller: 'AuthController',
    action: 'logout'
  },


  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  "GET *": {
    view: "homepage",
    skipAssets: true,
    skipRegex: /^\/api\/.*$/
  },

  /***************************************************************************
   *                                                                          *
   * Custom routes here...                                                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the custom routes above, it   *
   * is matched against Sails route blueprints. See `config/blueprints.js`    *
   * for configuration options and examples.                                  *
   *                                                                          *
   ***************************************************************************/

  /***************************************************************************
  *                                                                          *
  * QRCodes routes here...                                                    *
  *                                                                          *
  ***************************************************************************/
  'GET /api/qrcode/pdf/:idLocal': {
    controller: 'QrCodeController',
    action: 'getQrCodePdf'
  },

  'GET /api/qrcode/pdf': {
    controller: 'QrCodeController',
    action: 'getQrCodePdf'
  },


  /***************************************************************************
   *                                                                          *
   * Heroes routes here...                                                    *
   *                                                                          *
   ***************************************************************************/

  'GET /api/heroes': {
    controller: 'HeroController',
    action: 'getHeroes'
  },

  'GET /api/heroes/:name': {
    controller: 'HeroController',
    action: 'getHeroesByName',
    skipRegex: /^\/api\/heroes\/\d*$/
  },

  'GET /api/heroes/:id': {
    controller: 'HeroController',
    action: 'getHeroById'
  },

  'PUT /api/heroes': {
    controller: 'HeroController',
    action: 'update'
  },

  'POST /api/heroes': {
    controller: 'HeroController',
    action: 'insert'
  },

  'DELETE /api/heroes': {
    controller: 'HeroController',
    action: 'delete'
  },

  /***************************************************************************
   *                                                                          *
   * Computer routes here...                                                  *
   *                                                                          *
   ***************************************************************************/

  'GET /api/computers': {
    controller: 'ComputerController',
    action: 'getComputers'
  },

  'GET /api/computers/:local': {
    controller: 'ComputerController',
    action: 'getComputersOf'
  },

  'POST /api/computers': {
    controller: 'ComputerController',
    action: 'importComputers'
  },


  /***************************************************************************
  *                                                                          *
  * Bugs routes here...                                                      *
  *                                                                          *
  ***************************************************************************/


  'GET /api/bugs': {
    controller: 'BugController',
    action: 'getBugs'
  },

  'GET /api/bugs/:local': {
    controller: 'BugController',
    action: 'getBugsOf',
  },


  'PUT /api/bugs': {
    controller: 'BugController',
    action: 'updateBug'
  },

  'POST /api/bugs': {
    controller: 'BugController',
    action: 'insert'
  },

  'POST /api/bugs/uploadImage': {
    controller: 'BugController',
    action: 'uploadImage'
  },

  'GET /api/form/:name': {
    skipRegex: /^\/api\/form\/\d*$/
  }
};
