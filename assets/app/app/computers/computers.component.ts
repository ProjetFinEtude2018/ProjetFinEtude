import { Component, OnInit, ViewChild, Inject, Input, Optional, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { Computer } from '../models/computer';
import { ComputerService } from '../services/computer.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ComputersTableComponent } from '../computers-table/computers-table.component';


@Component({
  selector: 'app-computers',
  templateUrl: './computers.component.html',
  styleUrls: ['./computers.component.css']
})
export class ComputersComponent implements OnInit {

  locals: string[];
  myMap: Map<string, Computer[]>;

  @Input() file: any;

  @ViewChildren(ComputersTableComponent) childList: QueryList<ComputersTableComponent>

  constructor(private computerService: ComputerService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getComputers();
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ImportModalComponent, {
      data: this.file,
      height: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        var filename = result.name;
        var local = filename.substr(6, filename.length - 6 - 4);
        this.childList.find( (child) => child.local == local).getComputers();
      }
    });
  }

  getComputers(): void {
    this.computerService.getComputers().subscribe(
      computersMap => {
        this.myMap = new Map();
        for(var key in computersMap){
          this.myMap.set(key, []);
          for(var index in computersMap[key]){
            this.myMap.get(key).push(computersMap[key][index]);
          }
        }
        this.locals = Object.keys(computersMap).sort();
      });
  }

}

@Component({
  selector: 'app-import-modal',
  templateUrl: './import-computers-modal.html',
  styleUrls: ['./computers.component.css']
})
export class ImportModalComponent {

  file : File = null;

  constructor(
    public dialogRef: MatDialogRef<ImportModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private computerService: ComputerService) { }

  onChangeFile(files: FileList){
    this.file = files.item(0);
  }

  onSubmit() {
    if(this.file!=null) {
      this.computerService.importComputers(this.file)
      .subscribe((data) => {
          this.dialogRef.close(this.file);
        }
      );
    }
  }

}