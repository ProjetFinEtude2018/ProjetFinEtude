import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatFormFieldControl } from '@angular/material';

import { Computer } from '../models/computer';
import { ComputerService } from '../services/computer.service';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-computers-table',
  templateUrl: './computers-table.component.html',
  styleUrls: ['./computers-table.component.css']
})
export class ComputersTableComponent implements OnInit {

  @Input() local: string;
  @Input() computers : Computer[];
  
  displayedColumns = ['computerName', 'ipAddress', 'macAddress', 'comment', 'active'];
  dataSource = new MatTableDataSource<Computer>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private computerService: ComputerService) { }

  ngOnInit() {
    this.getComputers();
  }

  getComputers(){
    this.computerService.getComputersOf(this.local)
      .subscribe( (data) => {
        this.dataSource.data = data;
      });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
