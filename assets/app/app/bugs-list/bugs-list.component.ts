import { Component, OnInit } from '@angular/core';

import { BugService } from '../services/bug.service';




@Component({
  selector: 'app-bugs-list',
  templateUrl: './bugs-list.component.html',
  styleUrls: ['./bugs-list.component.css']
})
export class BugsListComponent implements OnInit {
  
  locals: string[];
 
  constructor(private bugService: BugService) {
  }

  ngOnInit() {
    this.getBugs();
  }

  getBugs(): void {
    this.bugService.getBugs().subscribe(bugs => {
      this.locals = Object.keys(bugs).sort();
    },
      error => console.log("getBugs in bug-list component has thrown an error\n"+ error));
  }
}
