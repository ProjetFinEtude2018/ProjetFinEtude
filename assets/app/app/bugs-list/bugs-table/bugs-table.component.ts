import { Component, Inject, Input, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSort, MatTableDataSource } from '@angular/material';

import { Bug, Status } from '../../models/bug';
import { BugService } from '../../services/bug.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription, ISubscription } from 'rxjs/Subscription';



@Component({
  selector: 'app-bugs-table',
  templateUrl: './bugs-table.component.html',
  styleUrls: ['./bugs-table.component.css']
})
export class BugsTableComponent implements OnInit {

  constructor(private bugService: BugService, public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) {

  }

  //
  @Input() local: string;
  bugsByLocal: Bug[];
  sub: any;
  /**
   * Code about managing the datatable comes here
   */
  dataSource = new MatTableDataSource<Bug>();
  displayedColumns = ['select', 'name_machine', 'email_author', 'description', 'createdAt', 'status'];

  /**
    * Code for sorting the datatable comes here
    */
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    /**
     * Managing how to sort our bugs datatable
     */
    this.dataSource.sortingDataAccessor = (data: Bug, property: string) => {
      switch (property) {
        case 'name_machine': return data.name_machine.name;
        case 'email_author': return data.email_author;
        case 'createdAt': return data.date_reporting;
        case 'status': return data.status;
        default: return '';
      }
    };

    this.getBugsOf(this.local);
  }

  /**
   * function that will open the modal with all the data
   * @param row the row with all the data to pass to the dialog modal
   */
  openDetails(row: any) {

    let dialogRef = this.dialog.open(DialogBugDetails, {
      width: '70%',
      data: {
        bugId: row.id,
        name_machine: row.name_machine.name,
        email_author: row.email_author,
        createdAt: row.createdAt,
        description: row.description,
        picture: row.picture,
        status: row.status,
        bug_solver: row.bug_solver
      }
    });
    dialogRef.beforeClose().subscribe(result => {
      this.getBugsOf(this.local);
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("closed");
      this.getBugsOf(this.local);
    });
  }


  getBugsOf(local: string): void {
    this.bugService.getBugsOf(local).subscribe(bugs => {
      this.bugsByLocal = bugs;
      this.dataSource.data = bugs;
      this.changeDetectorRefs.detectChanges();
    },
      error => console.log("getBugsOf in bug-table component has thrown an error\n" + error));
  }



}

/**
 * Code of the dialog bug details comes here
 */

@Component({
  selector: 'dialog-bug-details',
  templateUrl: 'dialog-bug-details.html',
  styleUrls: ['./dialog-bug-details.css']
})
export class DialogBugDetails {

  constructor(private bugService: BugService,
    public dialogRef: MatDialogRef<DialogBugDetails>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  /**
   * Function that will close the selected bug
   * @param bugId the id of the bug to close
   */
  closeBug(bug: Bug) {
    bug.status = Status.C;
    this.bugService.updateBug(bug).subscribe(
      success =>
        console.log("finish")
      ,
      error => console.log("getBugsOf in bug-table component has thrown an error\n" + error));
  }


  takeBug(bug: Bug) {
    bug.status = Status.IP;
    this.bugService.updateBug(bug).subscribe(
      success => 
     console.log("chips")
    ,
      error => console.log("getBugsOf in bug-table component has thrown an error\n" + error));
  }


}
