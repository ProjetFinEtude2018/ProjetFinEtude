import { Injectable } from '@angular/core';
import { API_ROUTES, COMPONENT_ROUTES } from '../../assets/config'

@Injectable()
export class ConfigService {


  constructor() { }

  getApiRoute(key) {
    return API_ROUTES[key];
  }

  getComponentRoute(key) {
    return COMPONENT_ROUTES[key];
  }

}
