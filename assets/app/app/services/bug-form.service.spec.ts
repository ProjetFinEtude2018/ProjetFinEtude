import { TestBed, inject } from '@angular/core/testing';

import { BugFormService } from './bug-form.service';

describe('BugFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BugFormService]
    });
  });

  it('should be created', inject([BugFormService], (service: BugFormService) => {
    expect(service).toBeTruthy();
  }));
});
