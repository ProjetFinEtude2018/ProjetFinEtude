import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { User } from '../models/User';
import { ConfigService } from './config.service';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private config: ConfigService, private router: Router) { }

  login(email:string, password:string): Observable<User[]> {
    
    return this.http.post<User[]>(this.config.getApiRoute("login"),{email,password} )
      .pipe(catchError(this.handleError<User[]>("login")));
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if(error.status===401)
        this.router.navigate(['/forbidden']);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
