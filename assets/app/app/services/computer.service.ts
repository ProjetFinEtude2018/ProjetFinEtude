import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';
import { Computer } from '../models/computer';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ComputerService {
  
  private computerUrl;

  constructor(private http: HttpClient, private config: ConfigService, private router:Router) {
    this.computerUrl = config.getApiRoute("computers");
  }

  /** GET computer from the server */
  getComputers (): Observable<Computer[]> {
    return this.http.get<Computer[]>(this.computerUrl)
      .pipe(catchError(this.handleError('getComputers', [])));
  }

  qrcode(): Observable<any> {
    let url = "/api/qrcode/pdf";
    return this.http.get<any>(url)
    .pipe(catchError(this.handleError("qrcode", [])));
  }

  /** GET computer from the server */
  getComputersOf (local): Observable<Computer[]> {
    let url = `${this.computerUrl}/${local}`;
    return this.http.get<Computer[]>(url)
      .pipe(catchError(this.handleError('getComputers', [])));
  }

  /** POST import computers to the server */
  importComputers (fileToUpload: File): Observable<Computer[]> {
    const formData: FormData = new FormData();
    formData.append('ipscan', fileToUpload, fileToUpload.name);
    return this.http.post<Computer[]>(this.computerUrl, formData)
      .pipe(catchError(this.handleError<Computer[]>("importComputers")));
  }

 /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if(error.status===401)
        this.router.navigate(['/forbidden']);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
