import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Role } from '../models/User';
import { Status } from '../models/bug';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Mr. Nice' },
      { id: 12, name: 'Narco' },
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' },
      { id: 16, name: 'RubberMan' },
      { id: 17, name: 'Dynama' },
      { id: 18, name: 'Dr IQ' },
      { id: 19, name: 'Magma' },
      { id: 20, name: 'Tornado' }
    ];
    const computers = [
        { name: 'LEN001', local: '017', ip: '192.168.16.1', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN002', local: '017', ip: '192.168.16.2', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN003', local: '017', ip: '192.168.16.3', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN004', local: '017', ip: '192.168.16.4', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN005', local: '019', ip: '192.168.16.5', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN006', local: '019', ip: '192.168.16.6', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN007', local: '019', ip: '192.168.16.7', mac_address: '9RJN19d10K10', comment: '', active: true},
        { name: 'LEN008', local: '019', ip: '192.168.16.8', mac_address: '9RJN19d10K10', comment: '', active: true}
    ];
    const users = [
        { id: 1, email: 'zakaria.lamrini@vinci.be', role: Role.A, password: '123456', user: true},
        { id: 2, email: 'ibrahim.mourade@vinci.be', role: Role.A, password: '123456', user: true},
        { id: 3, email: 'nicolas.christodoulou@vinci.be', role: Role.A, password: '123456', user: true},
        { id: 4, email: 'adeline.duterre@vinci.be', role: Role.A, password: '123456', user: true}
    ];
    const bugs = [
        {id: 1, email_author: 'relou@vinci.be', name_machine: computers[0], description: 'écran bleu', picture: '', status: Status.C, manager: users[0]},
        {id: 2, email_author: 'relou@vinci.be', name_machine: computers[1], description: 'ne demarre pas', picture: '', status: Status.IP, manager: users[1]},
        {id: 3, email_author: 'relou@vinci.be', name_machine: computers[2], description: 'un truc', picture: '', status: Status.P, manager: null},
        {id: 4, email_author: 'relou@vinci.be', name_machine: computers[0], description: 'augmentez la ram!!!', picture: '', status: Status.P, manager: null},
        {id: 5, email_author: 'relou@vinci.be', name_machine: computers[2], description: 'je sais plus', picture: '', status: Status.P, manager: null},
        {id: 6, email_author: 'relou@vinci.be', name_machine: computers[4], description: '..', picture: '', status: Status.IP, manager: users[3]},
        {id: 7, email_author: 'relou@vinci.be', name_machine: computers[7], description: '...', picture: '', status: Status.P, manager: null},
        {id: 8, email_author: 'relou@vinci.be', name_machine: computers[5], description: '.', picture: '', status: Status.P, manager: null},
    ];
    return {heroes, computers, users, bugs};
  }
}