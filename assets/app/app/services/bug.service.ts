import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { Bug } from '../models/bug';
import { ConfigService } from './config.service';
import { Router } from '@angular/router';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class BugService {

  constructor(private http: HttpClient, private config: ConfigService, private router: Router) { }

  /** GET heroes from the server */
  getBugs(): Observable<Bug[]> {
    return this.http.get<Bug[]>(this.config.getApiRoute("bugs"))
      .pipe(catchError(this.handleError('getBugs', [])));
  }

  getBugsOf(local: string): Observable<Bug[]> {
    let url = this.config.getApiRoute("bugs") + `/${local}`;
    return this.http.get<Bug[]>(url)
      .pipe(catchError(this.handleError('getBugsOf', [])));
  }

  updateBug(bug: Bug): Observable<Bug[]> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    let url = this.config.getApiRoute("bugs");
    console.log(bug);
    return this.http.put<Bug[]>(url, bug)
      .pipe(
      catchError(this.handleError('closeBug', [])));
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if(error.status===401)
        this.router.navigate(['/forbidden']);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  
}

