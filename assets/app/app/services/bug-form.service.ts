import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { ConfigService } from './config.service';
import { Bug } from '../models/bug';
import { Headers, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';

const httpOptions =  {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const imageHttpOptions = {
  headers: new HttpHeaders()
};

@Injectable()
export class BugFormService {

  private formUrl = "";
  private bugsUrl = "";
  private imageUrl = "";

  constructor(private http: HttpClient, private config: ConfigService, private router: Router) {
    this.formUrl = config.getApiRoute("form");
    this.bugsUrl = config.getApiRoute("bugs");
    this.imageUrl = config.getApiRoute("uploadImage");
  }

  /* POST : add a new bug to the database */
  addBug(bug: Bug) : Observable<Bug> {
    console.log("service !");
    return this.http.post<Bug>(this.bugsUrl, bug, httpOptions)
      .pipe(catchError(this.handleError<Bug>('addBug')));
  }

  /* GET : get the name of the computer that has a bug to report */
  getNameComputer(name: string) : Observable<string> {
    const url = `${this.formUrl}/${name}`;
    return this.http.get<string>(url, httpOptions)
      .pipe(catchError(this.handleError<string>(`getNameComputer name = ${name}`)));
  }

  /* POST : new file image */
  postFileImage(fileToUpload: File): Observable<string> {
    const endpoint = this.bugsUrl + "/uploadImage";
    const formData: FormData = new FormData();
    formData.append('imageBug', fileToUpload, fileToUpload.name);
    var imageBug = formData.get('imageBug');
    return this.http.post<string>(endpoint, formData)
      .pipe(catchError(this.handleError<string>(`putFileImage filename = ${fileToUpload.name}`)));
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if(error.status===401)
        this.router.navigate(['/forbidden']);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
