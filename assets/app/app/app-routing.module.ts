import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { ConfigService } from './services/config.service';
import { BugsListComponent } from './bugs-list/bugs-list.component';
import { ComputersComponent } from './computers/computers.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { LoginComponent } from './login/login.component';
import { BugFormComponent } from './bug-form/bug-form.component';

const config = new ConfigService();

const routes: Routes = [
  { path: '', redirectTo: config.getComponentRoute("default"), pathMatch: 'full' },
  { path: config.getComponentRoute("BugsListComponent"), component: BugsListComponent },
  { path: config.getComponentRoute("BugFormComponent"), component: BugFormComponent },
  { path: config.getComponentRoute("BugFormCompleted"), component: BugFormComponent },
  { path: config.getComponentRoute("ComputersComponent"), component: ComputersComponent },
  { path: config.getComponentRoute("LoginComponent"), component: LoginComponent },
  { path: config.getComponentRoute("ForbiddenComponent"), component: LoginComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
