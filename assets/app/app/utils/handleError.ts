import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

/**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
function handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if(error.status===401)
        this.router.navigate(['/forbidden']);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }