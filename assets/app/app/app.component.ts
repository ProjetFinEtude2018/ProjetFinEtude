import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ComputerService } from './services/computer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bug Reporting Tool';

  constructor(private router:Router, private computerService: ComputerService){

  }

  import(){
    this.router.navigate(["/computers"]);
  }

  consult(){
    this.router.navigate(["/bugs"]);
  }

  generate(){
    //this.computerService.qrcode().subscribe( (a) => console.log(a));
    //this.router.navigate(["/api/qrcode/pdf"]);
  }

}
