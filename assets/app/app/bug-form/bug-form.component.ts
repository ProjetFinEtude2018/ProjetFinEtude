import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BugFormService } from '../services/bug-form.service';
import { Http } from '@angular/http/src/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Bug } from '../models/bug';
import { AbstractControl } from '@angular/forms/src/model';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver/FileSaver';
import { ResponseType } from '@angular/http/src/enums';
import { Location } from '@angular/common';
//import { toast } from 'angular-toast';

@Component({
  selector: 'app-bug-form',
  templateUrl: './bug-form.component.html',
  styleUrls: ['./bug-form.component.css'],
  providers: [BugFormService]
})

export class BugFormComponent implements OnInit {

  bug: Bug;
  submitted: boolean;
  form: FormGroup;
  email: AbstractControl;
  name: AbstractControl;
  description: AbstractControl;
  image: AbstractControl;
  nameReceived: string = null;
  //reportingForm: HTMLFormElement = <HTMLFormElement>document.getElementById('reportingForm');
  fileToUpload: File;

  constructor(private bugFormService: BugFormService, public fbld: FormBuilder) {
    this.submitted = false;
    this.bug = new Bug();

    // Get the name_computer param of the request from the scan of the QR Code
    if (location.pathname.length > 5) {
      this.nameReceived = location.pathname.substring(6);
    }
    //console.log(this.nameReceived);

    // Add the validators for the required fields (+ email)
    this.form = fbld.group({
      'email': ['', Validators.compose([Validators.required,Validators.pattern(/^[a-z]+\.[a-z]+@(student\.)?vinci\.be$/)])],
      'name': ['', Validators.compose([Validators.required])],
      'description': ['', Validators.compose([Validators.required])],
      'image': ['', ]
    });
    this.email = this.form.controls['email'];
    this.name = this.form.controls['name'];
    this.description = this.form.controls['description'];
    this.image = this.form.controls['image'];
  }

  ngOnInit() {

  }

  // Error message for the field email
  getErrorMessageEmail() {
    return this.email.hasError('required') ? 'Veuillez entrer un email' :
      this.email.hasError('pattern') ? 'Email invalide' : '';
  }

  // Error message for the field name
  getErrorMessageName() {
    return this.name.hasError('required') ? 'Veuillez entrer un nom de machine' : '';
  }

  // Error message for the field description
  getErrorMessageDescription() {
    return this.name.hasError('required') ? 'Veuillez entrer une description' : '';
  }

  showToast() {

  }

  // Treatment button submit form
  onSubmit(e) {
    e.stopPropagation();
    e.preventDefault();
    this.submitted = true;
    //console.log(this.form.value.email);
    //console.log(this.form.value.name);
    //console.log(this.form.value.description);

    // if the fields of the form are valid
    if (this.form.valid) {
      this.bug.email_author = this.form.value.email;
      this.bug.name_machine = this.form.value.name;
      this.bug.description = this.form.value.description;
      console.log("Form submitted");  
      if (this.fileToUpload != null)
        this.upLoadFileToActivity();
      else this.bug.picture = "";
      this.addBug();
      this.form.reset();
    }
  }
  
  // Call addBug from the service to add the new bug in the db
  addBug() : void {
    this.bugFormService.addBug(this.bug)
      .subscribe(bugAdded => {
      console.log("done");
        this.bug.id = bugAdded.id
      });
  }

  handleFileInput(files : FileList) {
    this.fileToUpload = files.item(0);
  }

  upLoadFileToActivity() {
    console.log("Picture back " + this.bug.picture);
    this.bugFormService.postFileImage(this.fileToUpload)
      .subscribe(url => {this.bug.picture = url;});
  }

  getComputerName() : string {
    return this.nameReceived;
  }  

}
