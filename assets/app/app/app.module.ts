import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSortModule,
  MatToolbarModule,
  MatSidenavModule
} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BugsListComponent } from './bugs-list/bugs-list.component';
import { BugsTableComponent, DialogBugDetails } from './bugs-list/bugs-table/bugs-table.component';
import { ComputersTableComponent } from './computers-table/computers-table.component';
import {
  ComputersComponent,
  ImportModalComponent,
} from './computers/computers.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { BugService } from './services/bug.service';
import { ComputerService } from './services/computer.service';
import { ConfigService } from './services/config.service';
import { LoginService } from './services/login.service';
import { BugFormComponent } from './bug-form/bug-form.component';
import { BugFormService } from './services/bug-form.service';
import { FileUploadModule } from 'primeng/primeng'


//import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

/*import { ComputersComponent } from './computers/computers.component';
import { ComputerService } from './services/computer.service';*/


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    MatTableModule,
    HttpClientModule,
    MatExpansionModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatDialogModule,
    MatToolbarModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSidenavModule,
    FileUploadModule,
    FlexLayoutModule
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    /*HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )*/
  ],
  declarations: [
    AppComponent,
    NotFoundComponent,
    BugsListComponent,
    BugsTableComponent,
    DialogBugDetails,
    ComputersComponent,
    ComputersTableComponent,
    ImportModalComponent,
    ForbiddenComponent,
    ImportModalComponent,
    LoginComponent,
    BugFormComponent
  ],
  entryComponents: [
    DialogBugDetails,
    ImportModalComponent
  ],
  providers: [ ConfigService, ComputerService, BugService, LoginService, BugFormService,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
