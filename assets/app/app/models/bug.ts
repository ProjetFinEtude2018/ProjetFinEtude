import { User } from "./User";
import { Computer } from "./computer";

export enum Status {
    'P'="P",
    'IP'="IP",
    'C'="C"
}
export class Bug {
    id: number;
    email_author: string;
    name_machine: Computer;
    description: string;
    picture: string;
    status: Status;
    bug_solver: User;
    date_reporting: any;
}
  