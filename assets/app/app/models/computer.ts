export class Computer {
    name: string;
    local: string;
    ip: string;
    mac_address:string;
    comment:string;
    active:boolean;
}  