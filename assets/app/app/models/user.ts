export enum Role {
    'A'=0,
    'T'=1,
    'S'=2,
    'E'=3
}
export class User {
    id: number;
    email: string;
    role: Role;
    password:string;
}  