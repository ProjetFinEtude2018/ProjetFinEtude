export const API_ROUTES = {
    "heroes" : "api/heroes",
    "computers": "api/computers",
    "bugs" : "api/bugs",
    "uploadImage" : "api/bugs/uploadImage",
    "form" : "api/form",
    "login" : "login"
}

export const COMPONENT_ROUTES = {
    "default" : "/form",
    "HeroesComponent" : "heroes",
    "ComputersComponent" : "computers",
    "BugsListComponent" : "bugs",
    "BugFormComponent" : "form",
    "BugFormCompleted" : "form/:name",
    "ForbiddenComponent" : "forbidden",
    "LoginComponent" : "login"
}