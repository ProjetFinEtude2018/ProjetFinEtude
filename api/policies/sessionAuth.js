/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function (req, res, next) {
  let token;
  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      var scheme = parts[0],
        credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.json(401, res, "Format is Authorization: Bearer [token]");
    }
  } else if (req.signedCookies.adminAuth) {
    token = req.signedCookies.adminAuth;

    delete req.query.token;
  } else {
    return res.send(401);
  }

  JwtService.verify(token, function (err, decoded) {
    if (err) return res.send(401);
    req.token = token;
    User.findOne({ id: decoded.id }).then(function (user) {
      req.current_user = user;
      next();
    })
  });

}