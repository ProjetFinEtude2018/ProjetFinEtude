/**
 * Hero.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'hero',
  meta: {
     schemaName: 'pfe_db'
  },

  attributes: {
    name: {
      type: 'string'
    }
  },

  getAll: function(){
    return Hero.find();
  },

  getById: function(id){
    return Hero.findById(id);
  },

  getByName: function(name){
    return Hero.findByName({name:{'like': "%"+name+"%"}});
  },

  update: function(id, name){
    return Hero.update({id:id}, {name:name});
  },

  insert: function(name){
    return Hero.create({name:name});
  },

  delete: function(id){
    return Hero.destroy({id:id});
  }

};

