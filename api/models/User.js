/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require("bcrypt");
module.exports = {

  tableName: 'users',
  meta: {
    schemaName: 'pfe_db'
  },
  attributes: {
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    role: {
      type: 'string',
      enum: ['A', 'T', 'S', 'E'],
      defaultsTo: 'A'
    },
    password: {
      type: "string",
      minLength: 6,
      protected: true,
      required: true,
      columnName: "encryptedPassword"
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    },
    toJSON: function () {
      var obj = this.toObject()
      delete obj.password
    }
  },
  beforeCreate: function (values, cb) {
    bcrypt.hash(values.password, 10, function (err, hash) {
      if (err) return cb(err);
      values.password = hash;
      cb();
    });
  },
  comparePassword: function (password, user) {
    return new Promise(function (resolve, reject) {
      bcrypt.compare(password, user.password, function (err, match) {
        if (err) reject(err);

        if (match) {
          resolve(true);
        } else {
          reject(err);
        }
      })
    });
  }

};

