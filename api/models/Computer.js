/**
 * Computer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'computers',
    meta: {
        schemaName: 'pfe_db'
    },
    attributes: {
        name: {
            type: 'string',
            primaryKey: true
        },
        local: {
            type: 'string',
            required: true
        },
        ip: {
            type: 'string',
            required: true
        },
        mac_address: {
            type: 'string',
            required: true
        },
        comment: {
            type: 'text'
        },
        active: {
            type: 'boolean',
            required: true
        }
    },

    getByName: function(name) {
        return Computer.findByName({ name: { 'like': "%" + name + "%" } }).name;
    },

    getAll: function() {
        return Computer.find();
    },

    getByLocal: function(local) {
        return Computer.findByLocal(local);
    },

    insert: function(computer) {
        return Computer.create(computer);
    },

    createOrUpdate: function(computer) {
        return Computer.findOrCreate({ name: computer.name }, computer).then((result) => {
            return Computer.activate(result.name);
        });
    },

    activate: function(name) {
        return Computer.update({ name: name }, { active: true });
    },

    disable: function(local) {
        return Computer.update({ local: local }, { active: false });
    }
};