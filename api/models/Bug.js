/**
 * Bug.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'bugs',
    meta: {
        schemaName: 'pfe_db'
    },
    attributes: {
        email_author: {
            type: 'string',
            required: true
        },
        name_machine: {
            model: 'computer',
            required: true
        },
        description: {
            type: 'text',
            required: true
        },
        picture: {
            type: 'string'
        },
        status: {
            type: 'string',
            enum: ['P', 'IP', 'C'],
            defaultsTo: 'P'
        },
        bug_solver: {
            model: 'user'
        },
        date_reporting: {
            type: 'datetime',
        }

    },

    // Insert the new tuple of bugs in the db 
    insert: function(email, name, description, picture) {
        return Bug.create({ email_author: email, name_machine: name, description: description, picture: picture }).then();
    },

    // Update the bug with the image
    updateImage: function(id, imagePath) {
        return Bug.update({ id: id }, { picture: imagePath }).then();
    },

    getBugs: function() {
        return Bug.find().populate('name_machine');
    },
    getAllComplete: function(){
    return Bug.find().populate('bug_solver').populate('name_machine');
  }
};