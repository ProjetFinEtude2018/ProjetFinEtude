
/**
 * ComputerController
 *
 * @description :: Server-side logic for managing computers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    
    getComputers: function(req, res) {
        Computer.getAll().then(function(results){
            return res.json(_.groupBy(results,'local'));
        }).catch(function(e){
            return res.serverError(e);
        });
    },
    getComputersOf: function(req, res){
        let local = req.param("local");
        Computer.getByLocal(local).then(function(results){
            return res.json(results);
        }).catch(function(e){
            return res.serverError(e);
        });
    },
    importComputers: function(req, res) {
        var csv = require('csvtojson');
        console.log(req.file("ipscan"));
        req.file("ipscan").upload( (err, uploadedFiles) => {
            filename = uploadedFiles[0].filename;
            local = filename.substr(6, filename.length-4-6);
            Computer.disable(local).then( (result) => {
                csv({delimiter:';', headers:["ip", "name", "mac_address", "comment"]}).fromFile(uploadedFiles[0].fd)
                    .on('json', (jsonObj) => {
                        Computer.createOrUpdate({local: local, ip: jsonObj["ip"], mac_address: jsonObj["mac_address"],
                            comment: jsonObj["comment"], active: true, name: jsonObj["name"]});
                    })
                    .on('done', () => {
                        return res.ok();
                    })
                })
            });
                
    }

};