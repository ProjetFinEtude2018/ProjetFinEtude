/**
 * StatsController
 *
 * @description :: Server-side logic for managing stats
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	 /*Action who generate the .json needed to descriptive stats into charts*/
     getStats: 
     function(req, res){
        var retour=[];
        var data,caption,legende,bugs;
        var json=
        {
            "chart": {
                "caption": 'Rapport de bug',
                "startingangle": "120",
                "showlabels": "1",
                "showlegend": "1",
                "enablemultislicing": "0",
                "slicingdistance": "15",
                "showpercentvalues": "1",
                "showpercentintooltip": "0",
                "numberPrefix": "$",
                "theme": "ocean"
            },
            "data":'d'
        };
        Bug.getAllComplete().
        then(function(bugs)
        {
            switch(req.param("crit"))
            {
                case "solver":
                    data = countBySolver(bugs);
                    json.chart.caption="Bugs pris par les admin-sys";
                    json.chart.legende= "Admin-sys : $email Nombre total de bugs : $nbBug";
                    return data;
                break;
                default :
                    data = countByLocal(bugs);
                    json.chart.caption="Bugs par local";
                    json.chart.legende= "Local : $local Nombre total de bugs : $nbBug";
                    //console.log(data);
                    return data;
                break;
            }
        })
        .then(function(data)
        {
            json.data=data;
        })
        .then(function()
        {
            return res.json(json);
        });
    }
};

function countBySolver(array)
{
    var ret=[];var emails=[];
    for (var idx in array) {
        if(array[idx].bug_solver!=undefined)
        {
            obj={'label':array[idx].bug_solver.email,
                'value':1};
            if(!emails.includes(obj.label))
            {
                ret.push(obj);
                emails.push(obj.label);
            }
            else
            {
                key=emails.lastIndexOf(obj.label);
                ret[key].value++;
            }
        }else{continue;}
    }
    return ret;
};

function countByLocal(array)
{
    var ret=[];var locals=[];
    for (var idx in array) {
        if(array[idx].name_machine!=undefined)
        {
            obj={'label':array[idx].name_machine.local,'value':1};
            if(!locals.includes(obj.label))
            {
                ret.push(obj);
                locals.push(obj.label);
            }
            else
            {
                key=locals.lastIndexOf(obj.label);
                ret[key].value++;
            }
        }else{continue;}
    }
    return ret;
};