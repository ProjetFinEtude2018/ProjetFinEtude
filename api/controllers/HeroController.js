/**
 * HeroController
 *
 * @description :: Server-side logic for managing heroes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    getHeroes: function(req, res){
        Hero.getAll().then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    },

    getHeroById: function(req, res){
        let id = req.param("id");
        Hero.getById(id).then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    },

    getHeroesByName: function(req, res){
        let name = req.param("name");
        Hero.getByName(name).then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    },

    insert: function(req, res){
        let name = req.param("name");
        Hero.insert().then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    },

    update: function(req, res){
        let id = req.param("id");
        let name = req.param("name");
        Hero.update(id, name).then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    },

    delete: function(req, res){
        let id = req.param("id");
        Hero.delete(id).then(function(results){
            return res.json(results);
        }).catch(function(){
            return res.json('500');
        });
    }
	
};

