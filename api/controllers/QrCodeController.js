/**
 * QrCodeController
 *
 * @description :: Server-side logic for managing qrcodes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /*Action who generate the .pdf file with all QRCodes*/
    getQrCodePdf: function(req, res) {
        var qrcode = require('qrcode');
        var urlbase = process.env.LOCAL_URL + process.env.LOCAL_REPORTING_URL;
        var qrcodes_machines;
        var option, local;
        if (req.param("idLocal") == "all" || req.param("idLocal") == undefined) {
            option = "all";
        } else {
            option = "local";
            local = req.param("idLocal");
        }


        selectComputers(option, local, function(machines) {

                /*Computer.getAll()
                .then(function(machines)
                {
                */
                console.log(urlbase);
                console.log("3- " + machines[2]);
                var qrcodes_machines;
                createArrayQRCode(qrcode, urlbase, machines, function(qrcodes_machines) {
                    //return res.view('pdfTemplates/qrcode/pdf',{machines:qrcodes_machines});
                    sails.hooks.pdf.make("qrcode", { machines: qrcodes_machines, }, {
                        output: './assets/pdfs/qrcodes.pdf',
                        "border": {
                            "top": "1.5cm", // default is 0, units: mm, cm, in, px
                            "right": "1cm",
                            "bottom": "1.5cm",
                            "left": "1cm"
                        },
                    }).then(function(result) {
                        console.log("lala");
                        return res.sendfile('./assets/pdfs/qrcodes.pdf');
                    });
                });
            })
            .catch(function(err) {
                console.log('You have an error: ', err.stack);
                return res.json('500');
            });
    }
};

async function selectComputers(option, local, callback) {
    console.log("entrer dans la fonction");
    if (option == "local") {
        console.log("charger les PC d'un local");
        if (typeof(local) == 'undefined') {
            console.log("erreur 1");
            throw "Local needed !";
        }
        await Computer.getByLocal(local)
            .then(function(machines) {
                console.log("callback");
                callback(machines);
            })
            .catch(function(err) {
                console.log("erreur 2");
                throw err;
            });
    } else {
        console.log("Charger tous les pc");
        await Computer.getAll()
            .then(function(machines) {
                console.log("callback");
                console.log("1- " + machines[0]);
                callback(machines);
            });
    }
}


/* Fill an array with URL adresses of QRCodes images generate
PRE : qrcode : the core managing the QRCode genération
        urlbase : the base of URL to go with QRCode
        array_machines : Array of machine object
        array_return : array filled by fonction with adresses
        callback : fonction of callback.

POST : sends array_return filled with QRCodes adresses generated which url adresses bind urlbase/elt
*/
function createArrayQRCode(qrcode, urlbase, arr_machines, callback) {
    var array_return = [];
    var x = 0;
    var loopArray = function(arr_machines) {
            // call itself
            var url = urlbase + "/" + arr_machines[x].name;
            url2QRCode(qrcode, url, function(ret) {
                array_return.push({ name: arr_machines[x].name, qrcode: ret });
                // set x to next item
                x++;
                // any more items in array?
                if (x < arr_machines.length) { loopArray(arr_machines); }
                // if no more items.
                else { callback(array_return); }
            });
        }
        // launch 'loop'
    loopArray(arr_machines);
}


/* generate the adress of a QRCode image from an url.
PRE : qrcode : the core managing the QRCode genération
        url : the url linked to the QRCode
        callback : fonction of callback, with a parameter.

POST : sends the path of QRCode image in parameter of callback.
*/
function url2QRCode(qrcode, url, callback) {

    // fancy code to show your message
    qrcode.toDataURL(url, function(err, qrcodePath) {

        // do callback when ready
        callback(qrcodePath);
    });
}