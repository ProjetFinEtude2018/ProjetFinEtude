module.exports = {

    login: function (req, res) {
      var email = req.param('email');
      var password = req.param('password');
  
      verifyParams(res, email, password)
  
      User.findOne({email: email}).then(function (user) {
        if (!user) {
          return invalidEmailOrPassword(res);
        }
        signInUser(req, res, password, user);
        
      }).catch(function (err) {
        return invalidEmailOrPassword(res);
      })
    },
    logout: function (req, res) {
      res.clearCookie('adminAuth');
      res.clearCookie('user');
      req.session.destroy();
      console.log("deconnected");
      res.ok();
    }
  
  };
  
  
  function signInUser(req, res, password, user) {  
    User.comparePassword(password, user).then(
      function (valid) {
        if (!valid) {
          return this.invalidEmailOrPassword();
        } else {
          var responseData = {
            user: user,
            token: generateToken(user.id)
          }
          console.log("user connected");
          res.cookie('adminAuth', token, { signed: true, httpOnly : true });
          res.cookie('userID', user.id, { signed: true, httpOnly : true });
          return ResponseService.json(200, res, "Successfully signed in", responseData)
        }
      }
    ).catch(function (err) {
      return ResponseService.json(403, res, "Forbidden")
    })
  };
  
  
  function invalidEmailOrPassword(res){  
    return ResponseService.json(401, res, "Invalid email or password")
  };
  
  function verifyParams(res, email, password){  
    if (!email || !password) {
      return ResponseService.json(401, res, "Email and password required")
    }
  };
  
  
  function generateToken(user_id) {  
    return JwtService.issue({id: user_id})
  };