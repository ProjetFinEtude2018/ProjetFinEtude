/* jshint esversion: 6 */
var dateTime = require('date-time');

/**
 * BugController
 *
 * @description :: Server-side logic for managing bugs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getBugs: function(req, res) {
        Bug.getBugs().then(function(results) {
            var groupedData = _.groupBy(results, function(d) { return d.name_machine.local });
            return res.json(groupedData);
        }).catch(function(e) {
            return res.serverError(e);
        });
    },

    getBugsOf: function(req, res) {
        let local = req.param("local");
        Bug.getBugs().then(function(results) {
            results = _.filter(results, function(bug) { return bug.name_machine.local == local });
            return res.json(results);
        }).catch(function(e) {
            return res.serverError(e);
        });
    },

    updateBug: function(req, res) {

        let bugId = req.body['bugId'];
        let bugSolver = req.body['bug_solver'];
        let bugStatus = req.body['status'];
        if(bugStatus == "IP"){
            bugSolver = req.signedCookies.userID
        }
        Bug.update({ id: bugId }, {
            bug_solver: bugSolver,
            status: bugStatus
        }).then(function(results) {
            res.ok();
        }).catch(function(e) {
            return res.serverError(e);
        });
    },

    // Call the insert method from the Bug with the requested params
    insert: function(req, res) {
        let email = req.param("email_author");
        let name = req.param("name_machine");
        let description = req.param("description");
        let picture;
        if (req.param("picture") != "")
            picture = "assets/images/reports/" + name + "_" + dateTime() + ".jpg";
        else picture = null;

        Bug.insert(email, name, description, picture).then(function(results) {
            return res.json(results);
        }).catch(function(e) {
            console.log(e);
            return res.serverError(e);
        });
    },

    // Upload the picture on the server
    uploadImage: function(req, res) {
        var fs = require('fs');
        const imagePath = 'assets/images/reports';
        var imageUrl;
        var uploadFile = req.file('imageBug');
        console.log(uploadFile);

        uploadFile.upload({
            dirname: imagePath,
        }, function onUploadComplete(err, uploadedFiles) {
            imageUrl = uploadedFiles[0].filename;
            console.log(imageUrl);
            fs.rename(uploadedFiles[0].fd, imagePath + "/" + uploadedFiles[0].filename, () => {});
            return imageUrl;
        });
    }
};